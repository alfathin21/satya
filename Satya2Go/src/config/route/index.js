import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import {
  Home,
  SplashScreen,
  Login,
  Inventory,
  Items,
  ScanQR,
  Listitem,
  SearchItem,
} from '../../containers/pages/index';

const SplashScreenStack = createStackNavigator(
  {
    SplashScreen,
    Login,
  },
  {
    headerMode: 'none',
    initialRouteName: 'SplashScreen',
  },
);

const HomeStack = createStackNavigator(
  {
    Home,
    ScanQR
  },
  {
    headerMode: 'none',
    initialRouteName: 'Home',
  },
);


const InventoryStack = createStackNavigator(
  {
    Inventory,
    Items,
    Listitem,
    SearchItem,

  },
  {  
    headerMode:'screen',
    initialRouteName: 'Inventory',
    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: '#1E90FF',
      },
       headerTintColor:'#FFF',
       headerTitleAlign:'center'
    }
  },
);

const ItemsStack = createStackNavigator(
  {
    Items,

  },
  {
    initialRouteName: 'Items',
  },
);

const ScanQRStack = createStackNavigator(
  {
    ScanQR,
  },
  {
    headerMode: 'none',
    initialRouteName: 'ScanQR',
  },
);


const ListitemStack = createStackNavigator(
  {
    Listitem,

  },
  {
    initialRouteName: 'Listitem',
  },
);

const SearchItemStack = createStackNavigator(
  {
    SearchItem,

  },
  {
    initialRouteName: 'SearchItem',
  },
);



const Router = createSwitchNavigator(
  {
    HomeStack,
    SplashScreenStack,
    InventoryStack,
    ItemsStack,
    ListitemStack,
    SearchItemStack,
    ScanQRStack
  },
  {
    headerMode: 'none',
    initialRouteName: 'SplashScreenStack',
  },
);

export default createAppContainer(Router);
