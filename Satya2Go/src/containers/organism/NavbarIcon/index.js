import React, { Component } from 'react';
import { View, Image, StyleSheet, TouchableOpacity, TouchableWithoutFeedback, Text, Alert } from 'react-native';
import {withNavigation} from 'react-navigation';


class NavbarIcon extends Component {
    render() {
        return (
            <View>
                <View style={{
                    position: 'absolute',
                    alignSelf: 'center',
                    backgroundColor: '#FFF',
                    width: 70,
                    height: 70,
                    borderRadius: 35,
                    bottom: 12,
                    zIndex: 10
                }}>
                    <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('ScanQR')}>
                        <View style={[styles.button, styles.actionBtn]}>
                            <Image style={{ width: 50, height: 50 }}
                                resizeMode="contain"
                                source={require('../../../assets/icons/scanQR.png')} 
                                onPress={() => this.props.navigation.navigate('ScanQR')}
                                />                                
                        </View>
                    </TouchableWithoutFeedback>
                </View>
                <View style={{

                    position: 'absolute',
                    backgroundColor: '#1E90FF',
                    border: 6,
                    radius: 3,
                    shadowOpacity: 0.3,
                    shadowRadius: 3,
                    shadowOffset: {
                    height: 3, width: 3
                    },
                    x: 0,
                    y: 0,
                    style: { marginVertical: 5 },
                    bottom: 0,
                    width: '100%',
                    height: 50,
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    paddingVertical: 10,
                    paddingHorizontal: 25}}>
                    <View style={{
                        flexDirection: 'column', alignItems: 'center', justifyContent: 'center'
                    }}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Home')}>
                            <Image
                                style={{ width: 23, height: 23 }}
                                source={require('../../../assets/icons/Homes.png')}>
                            </Image>
                        </TouchableOpacity>
                        <Text style={{justifyContent:'center',color:'white',alignItems:'center', fontSize:12}}>Home</Text>
                    </View>                   
                    <View style={{
                        flexDirection: 'column', alignItems: 'center',justifyContent:'center',marginStart:30
                    }}>

                        <TouchableOpacity
                            onPress={() => this.props.navigation.navigate('Items')}
                        >
                            <Image
                                style={{  width: 23, height: 23 }}
                                source={require('../../../assets/icons/list.png')}
                            />
                       
                        </TouchableOpacity>
                        <Text style={{justifyContent:'center',color:'white',alignItems:'center' , fontSize:12}}>List</Text>
                    </View>

                        <View style={{
                             flexDirection: 'column', alignItems: 'center',justifyContent:'center',marginStart:85,
                        }}>
                            <TouchableOpacity
                                onPress={() => this.props.navigation.navigate('SearchItem')}>
                                <Image
                                    source={require('../../../assets/icons/search.png')}
                                    style={{ marginHorizontal: 16, width: 22, height: 22 }}
                                    containerStyle={{ marginHorizontal: 16 }}/>
                            </TouchableOpacity>
                            <Text style={{justifyContent:'center',color:'white',alignItems:'center', fontSize:12 }}>Search</Text>
                        </View>
                        <View style={{
                            flexDirection: 'column', alignItems: 'center',justifyContent:'center',
                          
                        }}>
                            <TouchableOpacity
                                onPress={() => this.props.navigation.navigate('Listitem')}>
                                <Image
                                    source={require('../../../assets/icons/history.png')}
                                    style={{ marginHorizontal: 16, width: 23, height: 23}}
                                    containerStyle={{ marginHorizontal: 16 }}
                                />                     
                            </TouchableOpacity>
                            <Text style={{justifyContent:'center',color:'white',alignItems:'center', paddingLeft:7, fontSize:12 }}>ItemList</Text>                           
                        </View>

                    {/* </View> */}
                </View>
            </View>
        );
    }

    
}
export default withNavigation(NavbarIcon);

const styles = StyleSheet.create({

    MainContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'blue'
    },
    button: {
        width: 60,
        height: 60,
        alignItems: 'center',
        justifyContent: 'center',
        shadowColor: 'grey',
        shadowOpacity: 0.1,
        shadowOffset: { x: 2, y: 0 },
        shadowRadius: 2,
        borderRadius: 30,
        position: 'absolute',
        bottom: 20,
        right: 0,
        top: 5,
        left: 5,
        shadowOpacity: 5.0,

    },
    actionBtn: {

        backgroundColor: '#1E90FF',
        textShadowOffset: { width: 5, height: 5 },
        textShadowRadius: 10,
        borderWidth: 2,
        borderColor: '#fff'


    }


});
