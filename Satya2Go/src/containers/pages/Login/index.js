import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  Dimensions,
  StatusBar,
  TouchableOpacity,
  TextInput,
  AsyncStorage
} from 'react-native';

const userInfo = {username:'2019100003', password:'satyadmok3'} 


class Login extends Component {
  static navigationOptions = {
      headerShown : 'fasle'
  }

  constructor (props) {
    super(props);
    this.state = {
      username:'',
      password:'',
      isLoading: true
    }
  }

  performTimeConsumingTask = async() => {
    return new Promise((resolve) =>
      setTimeout(
        () => { resolve('result') },
        2000
      )
    );
  }
  async componentDidMount() {
    const data = await this.performTimeConsumingTask();
    if (data !== null) {
      this.setState({ isLoading: false });
    }
  }

  static navigationOptions = {
    headerShown:false
   };

  render() {
    return (
      <View
        style={{
          flex: 1,
          justifyContent: 'flex-end',
        }}>
        <StatusBar
          barStyle='light-content'
          hidden={false}
          backgroundColor="#000"
        />
        <View style={{...StyleSheet.absoluteFill}}>
          <Image
            source={require('../../../assets/Image/bgLogin.png')}
            style={{flex: 1, height: null, width: null}}
            resizeMode='stretch'
          />
        </View>
        <View style={{flex:1, justifyContent:'flex-start',alignItems:'center',marginTop:15}}>
          <Image style={{width:110,height:110}}
          source={require('../../../assets/Image/Satya2Go.png')} />
        </View>
        <View style={styles.container}>
          <TextInput style={styles.inputBox}
              underlineColorAndroid='rgba(0,0,0,0)'
              placeholder="Username"
              placeholderTextColor = "#ffffff"
              selectionColor="#fff"
              keyboardType="email-address"
              onChangeText={(username) => this.setState({username})}
              value={this.state.username}
              autoCapitalize="none"
              />
          <TextInput style={styles.inputBox}
              underlineColorAndroid='rgba(0,0,0,0)'
              placeholder="Password"
              secureTextEntry={true}
              placeholderTextColor = "#ffffff"
              onChangeText={(password) => this.setState({password})}
              value={this.state.password}
              />
           <TouchableOpacity style={styles.button} onPress={this._signin}>
             <Text style={styles.buttonTextSubmit}>LOGIN</Text>
           </TouchableOpacity>
  		</View>
      </View>
    );
  }


  _signin = async () => {
    let details = {
      'username': this.state.username,
      'password': this.state.password
  };
  
 
  let formBody = [];
      for (let property in details) {
          let encodedKey = encodeURIComponent(property);
          let encodedValue = encodeURIComponent(details[property]);
          formBody.push(encodedKey + "=" + encodedValue);
      }
      formBody = formBody.join("&");
      fetch('http://admin-2go.satyadm.co.id/service/auth/users', {
        method: 'POST',
        headers: {
            'Authorization': 'Bearer token',
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        body: formBody
    }).then((response) => response.json())
        .then((responseData) => {
          if (this.state.username == '' || this.state.password == '') {
            setTimeout(() => {
              alert('Silahkan isi username & password anda !');
            }, 100); 
            return false;
          } else if (responseData.status != false) {
                  this.props.navigation.navigate('Home')
                }else {
                    this.setState({ spinner: false });
                    setTimeout(() => {
                      alert('Username & Password Salah!');
                    }, 100); 
                }
        })
        .done();
  }




  
    // _login = async () => {
    //   if(userInfo.username === this.state.username && userInfo.password === this.state.password) {
    //     // alert('logged in');
    //     this.props.navigation.navigate('Home')
    //   }else {
    //     // alert('Username atau Password salah.')
    //     this.props.navigation.navigate('Home')
    //   }
    // }

}
export default Login;

const {width, height} = Dimensions.get('window');

const styles = StyleSheet.create({
  container : {
    flexGrow: 1,
    justifyContent:'center',
    alignItems: 'center'
  },

  inputBox: {
    width:300,
    backgroundColor:'rgba(255, 255,255,0.2)',
    borderRadius: 25,
    paddingHorizontal:16,
    fontSize:16,
    color:'#ffffff',
    marginVertical: 10
  },
  button: {
    width:300,
    backgroundColor:'rgba(255, 255,255,0.2)',
     borderRadius: 25,
      marginVertical: 10,
      paddingVertical: 13
  },
  buttonText: {
    fontSize:16,
    fontWeight:'500',
    color:'#ffffff',
    textAlign:'center'
  },
  buttonTextSubmit: {
    fontSize:16,
    fontWeight:'500',
    color:'#EAEAEA',
    textAlign:'center',
  }

});