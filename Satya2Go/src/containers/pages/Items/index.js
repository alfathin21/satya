import React, { Component } from 'react';
import { StyleSheet, Text, View, FlatList, Image, TouchableOpacity, ActivityIndicator } from 'react-native';

function Item({ item }) {
  return (
    <View style={styles.listItem}>
      <TouchableOpacity>
      <Image source={require('../../../assets/icons/ScanQR1.png')}  style={{width:60, height:60,borderRadius:30, marginTop:10}} />
      </TouchableOpacity>
      <View style={{alignItems:"center",flex:1}}>
        <Text style={{fontWeight:"bold"}}>{item.kode}</Text>
        <Text>{'PLU ' + item.plu}</Text>
        <Text>{item.nama}</Text>
        <Text>{'Price : ' + item.harga}</Text>
      </View>
        <View style={{backgroundColor:'#FFF',width: 45, height: 45, borderRadius: 40,textAlign:'center',alignContent:'center',marginTop:20}}>
        <Text style={{textAlign:'center',color:'#1E90FF',marginTop:12}}>{item.jumlah}</Text>
        </View>
    </View>
  );
}

export default class Items extends Component {

  constructor(props){
    super(props);
    this.state = { 
      loading: false,
      error: null
    }
  }

  componentDidMount(){
      fetch('http://admin-2go.satyadm.co.id/service/product/barang')
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({
          isLoading: true,
          dataSource: responseJson,
        }, function(){

        });

      })
      .catch((error) =>{
        console.error(error);
      });
  }


  render(){
    if (this.state.loading) {
      return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
          <ActivityIndicator />
        </View>
      );
    }
    return (
      <View style={styles.container}>
      <FlatList
        style={{flex:1}}
        data={this.state.dataSource}
        renderItem={({ item }) => <Item item={item}/>}
        keyExtractor={item => item.no}
      />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF',
    marginTop:2
  },
  listItem:{
    margin:10,
    padding:10,
    backgroundColor:"#EAEAEA",
    width:"95%",
    flex:1,
    alignSelf:"center",
    flexDirection:"row",
    borderRadius:5
  }
});
