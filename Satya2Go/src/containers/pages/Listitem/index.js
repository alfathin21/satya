import React from 'react';
import { StyleSheet, Text, View, FlatList, Image, TouchableOpacity } from 'react-native';

function Item({ item }) {
  return (
    <View style={styles.listItem}>
      <TouchableOpacity>
      <Image source={require('../../../assets/icons/ScanQR1.png')}  style={{width:60, height:60,borderRadius:30}} />
      </TouchableOpacity>
      <View style={{alignItems:"center",flex:1}}>
        <Text style={{fontWeight:"bold"}}>{item.name}</Text>
        <Text>{item.position}</Text>
      </View>
      <TouchableOpacity>
        <View style={{backgroundColor:'#FFF',width: 45, height: 45, borderRadius: 40,textAlign:'center',alignContent:'center', marginTop:8}}>
        <Text style={{textAlign:'center',color:'#1E90FF', marginTop:10}}>{item.photo}</Text>
        </View>
      </TouchableOpacity>
    </View>
  );
}

export default class ListItem extends React.Component {
  state = {
    data:[
        {
            "name": "MAC-SPK-ZA-SN",
            "email": "miyah.myles@gmail.com",
            "position": "Spring Bulat SN",
            "photo": "300"
        },
        {
            "name": "MAC-SPK-ZA-SN",
            "email": "june.cha@gmail.com",
            "position": "Spring Kotak SN",
            "photo": "654"
            
        },
        {
            "name": "MAC-SPK-ZA-SN",
            "email": "iida.niskanen@gmail.com",
            "position": "Spring Segitiga SNr",
            "photo": "234"
        },
        {
            "name": "MAC-SPK-ZA-SN",
            "email": "renee.sims@gmail.com",
            "position": "Spring Oval SN",
            "photo": "753"
        },
        {
            "name": "JMAC-SPK-ZA-SN",
            "email": "jonathan.nu\u00f1ez@gmail.com",
            "position": "Spring Travesium SN",
            "photo": "345"
        },
        {
            "name": "Sasha Ho",
            "email": "MAC-SPK-ZA-SN",
            "position": "Spring Jajar SN",
            "photo": "432"
        },
        {
            "name": "MAC-SPK-ZA-SN",
            "email": "abdullah.hadley@gmail.com",
            "position": "Spring Lonjong SN",
            "photo": "435"
        },
        {
            "name": "MAC-SPK-ZA-SN",
            "email": "thomas.stock@gmail.com",
            "position": "Product Designer",
            "photo": "745"
        },
        {
            "name": "MAC-SPK-ZA-SN",
            "email": "veeti.seppanen@gmail.com",
            "position": "Spring Bulat SN",
            "photo": "231"
        },
        {
            "name": "MAC-SPK-ZA-SN",
            "email": "bonnie.riley@gmail.com",
            "position": "Spring Lonjong",
            "photo": "235"
        }
    ]
  }


  render(){
    return (
      <View style={styles.container}>
      <FlatList
        style={{flex:1}}
        data={this.state.data}
        renderItem={({ item }) => <Item item={item}/>}
        keyExtractor={item => item.email}
      />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF',
    marginTop:2
  },
  listItem:{
    margin:10,
    padding:10,
    backgroundColor:"#EAEAEA",
    width:"90%",
    flex:1,
    alignSelf:"center",
    flexDirection:"row",
    borderRadius:5
  }
});
