import React, {Component,Fragment} from 'react';
import {Text, View, TouchableOpacity, Image, Dimensions, ActivityIndicator,Linking,StatusBar} from 'react-native';
import QRCodeScanner from 'react-native-qrcode-scanner';
import styles from './scanStyle';
import {
  Header,
  Colors,
} from 'react-native/Libraries/NewAppScreen';
const { width } = Dimensions.get('screen');
const IconWithText = (props) => {
  return (
    <TouchableOpacity>
    <View>
      <Image style={{width: 90, height: 90, borderRadius: 90}} source={props.img} />
  <Text style={{maxWidth:60, textAlign:'center', marginLeft:6,fontWeight:'bold'}}>{props.title}</Text>
    </View>
    </TouchableOpacity>
  )
}

const IconAction = (props) => {
  return (
    <TouchableOpacity>
    <View>
      <Image style={{width: 56, height: 56, borderRadius: 55}} source={props.img} />
    </View>
    </TouchableOpacity>
     
     )
}
class ScanQode extends Component {
  constructor(props) {
      super(props);
      this.state = {
          scan: false,
          ScanResult: false,
          result: null,
          value:'',
          dataSource: [],
          kode_barang:'',
          nama_barang:'',
          plu:'',
          price:'',
          jumlah:''
      };
  }
  getRemoteData = async (data) => {
    let details = {
        'id': data
  };
  let formBody = [];
      for (let property in details) {
          let encodedKey = encodeURIComponent(property);
          let encodedValue = encodeURIComponent(details[property]);
          formBody.push(encodedKey + "=" + encodedValue);
      }
      formBody = formBody.join("&");
      fetch('http://admin-2go.satyadm.co.id/service/product/items', {
        method: 'POST',
        headers: {
            'Authorization': 'Bearer token',
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        body: formBody
    }).then((response) => response.json())
        .then((responseData) => {
          if (responseData != '') {
            this.setState({
              kode_barang:responseData.kode_barang,
              nama_barang:responseData.nama_barang,
              plu:responseData.plu,
              jumlah:responseData.jumlah,
              price:responseData.price
              })
          }
        })
        .done();
  }

  onSuccess = (e) => {
      const check = e.data.substring(0, 4);
      console.log('scanned data' + check);
      this.setState({
      scan: false,
      ScanResult: true
      })
      if (check === 'http') {
          Linking
              .openURL(e.data)
              .catch(err => console.error('An error occured', err));
      } else {
        this.setState({
          scan: false,
          ScanResult: true
          })
          const data = e.data;
          this.getRemoteData(data);
      }
  }
  activeQR = () => {
      this.setState({
          scan: true
      })
  }
  scanAgain = () => {
      this.setState({
          scan: true,
          ScanResult: false,
          kode_barang:'',
          nama_barang:'',
          plu:'',
          price:'',
          jumlah:''
      })
  }
  render() {
      const { scan, ScanResult, result } = this.state
      const desccription = 'Silahkan anda melakukan scand Barcode untuk melihat Detail !'
      return (
          <View style={styles.scrollViewStyle}>
              <Fragment>
                  <StatusBar barStyle="dark-content" />
                  <Text style={styles.textTitle}>Barcode Scanner Product Items</Text>
                  {!scan && !ScanResult &&
                      <View style={styles.cardView} >
                          <Text numberOfLines={8} style={styles.descText}>{desccription}</Text>
                          <TouchableOpacity onPress={this.activeQR} style={styles.buttonTouchable}>
                              <Text style={styles.buttonTextStyle}>Click to Scan !</Text>
                          </TouchableOpacity>
                      </View>
                  }

                  {ScanResult &&
                      <Fragment>
                        <Text style={styles.textTitle1}>Result !</Text>
                        <View style={ScanResult ? styles.scanCardView : styles.cardView}>
                        <Text>Kode Barang : {` ${this.state.kode_barang}`}</Text>
                        <Text>Nama Barang :{` ${this.state.nama_barang}`}</Text>
                        <Text>PLU: {` ${this.state.plu}`}</Text>
                        <Text>Price : {` ${this.state.price}`}</Text>
                        <Text>Jumlah : {` ${this.state.jumlah}`}</Text>
                        <TouchableOpacity onPress={this.scanAgain} style={styles.buttonTouchable}>
                            <Text style={styles.buttonTextStyle}>Lakukan Scan Lagi</Text>
                        </TouchableOpacity>
                          </View>
                      </Fragment>
                  }
                  {scan &&
                      <QRCodeScanner
                          reactivate={true}
                          showMarker={true}
                          ref={(node) => { this.scanner = node }}
                          onRead={this.onSuccess}
                          topContent={
                              <Text style={styles.centerText}>
                                  <Text style={styles.textBold}>Dekatkan Barcode</Text> </Text>
                          }
                          bottomContent={
                              <View>
                                  <TouchableOpacity style={styles.buttonTouchable} onPress={() => this.scanner.reactivate()}>
                                      <Text style={styles.buttonTextStyle}>OK. Mengerti!</Text>
                                  </TouchableOpacity>

                                  <TouchableOpacity style={styles.buttonTouchable} onPress={() => this.setState({ scan: false })}>
                                      <Text style={styles.buttonTextStyle}>Stop Scan</Text>
                                  </TouchableOpacity>
                              </View>

                          }
                      />
                  }
              </Fragment>
          </View>

      );
  }
}



export default ScanQode;



// class ScanQode extends Component {
  
// //   constructor(props) {
// //     super(props);
// //     this.state = {
// //         scan: false,
// //         ScanResult: false,
// //         result: null
// //     };
// // }
//     constructor(props) {
//       super(props);
    
//       this.state = {
//         loading: false,
//         dataSource: [],
//         error: null,
//         barcode:'',
//         qrvalue: '',
//         opneScanner: false,
//         scan: false,
//         ScanResult: false,
//         result: null
//       };
    
//       this.arrayholder = [];
//     }

// // onSuccess = (e) => {
// //     const check = e.data.substring(0, 30);
// //     this.setState({ barcode: check });
// // }


// onSuccess = (e) => {
//   const check = e.data.substring(0, 4);
//   console.log('scanned data' + check);
//   this.setState({
//       result: e,
//       scan: false,
//       ScanResult: true
//   })
//   if (check === 'http') {
//       Linking
//           .openURL(e.data)
//           .catch(err => console.error('An error occured', err));


//   } else {
//       this.setState({
//           result: e,
//           scan: false,
//           ScanResult: true
//       })
//   }

// }

// activeQR = () => {
//   this.setState({
//       scan: true
//   })
// }
// scanAgain = () => {
//   this.setState({
//       scan: true,
//       ScanResult: false
//   })
// }



// componentDidMount() {
//           this.makeRemoteRequest();
//         }        
//         makeRemoteRequest = () => {
//           const url = `http://admin-2go.satyadm.co.id/service/product/items`;
//           this.setState({ loading: true });
        
//           fetch(url, {
//             method: 'POST',
//             headers: {
//               'Accept': 'application/json',
//               'Content-Type': 'application/json',
//             },
//             body: JSON.stringify({
//               // id & Value Scan Manual input 
//               id:'29301683;PPD2020020073;1',
//             })
//           })
//           .then (function(response){
//            //   console.log(response)
//             })
//             .then((response) => response.json())
//             .then((responseJson) =>{
//               this.setState({
//                 dataSource: responseJson,
//                 error: Datares.error || null,
//                 loading: false,
//               });
//               this.arrayholder = dataSource;
//             })
//             .catch(error => {
//               this.setState({ error, loading: false });
//             });
//           };  

//   render(){
//     let displayModal;
//   if (this.state.loading) {
//     return (
//       <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
//         <ActivityIndicator />
//       </View>
//     );
//   }
//   return (
//     <View style={{flex: 1}}>
//       {/* Scan Camera Area */}
//         <View style={{flex: 1}}>
//             <QRCodeScanner
//                 reactivate={false}
//                 showMarker={true}
//                 ref={(node) => { this.scanner = node }}
//                 onRead={this.onSuccess}
//                 topContent={
//                   <Text style={styles.centerText}>
//                       Go to <Text style={styles.textBold}>wikipedia.org/wiki/QR_code</Text> on your computer and scan the QR code to test.</Text>
//               }
//               bottomContent={
//                 <View>
//                     <TouchableOpacity style={styles.buttonTouchable} onPress={() => this.scanner.reactivate()}>
//                         <Text style={styles.buttonTextStyle}>OK. Got it!</Text>
//                     </TouchableOpacity>

//                     <TouchableOpacity style={styles.buttonTouchable} onPress={() => this.setState({ scan: false })}>
//                         <Text style={styles.buttonTextStyle}>Stop Scan</Text>
//                     </TouchableOpacity>
//                 </View>

//             }
              
//           //    reactivateTimeout={1}
//                 />
//           <View style={{flexDirection:'row', paddingHorizontal: 16, justifyContent: 'space-between', marginTop: 25, position:'absolute',
//           top:0,
//           left:0, width:'100%'}}>
    
//                  <IconAction img={require('../../../assets/icons/back.png')}/>
       
//             <View style={{flexDirection: 'row', justifyContent:'space-between', width:110,}}>
//               <IconAction img={require('../../../assets/icons/image.png')}/>
//               <IconAction img={require('../../../assets/icons/star.png')}/>
//             </View>
//           </View>
      
//         </View>
//         {/* Result Area Start Container */}
//         <View style={{height: 210, backgroundColor: 'white', paddingHorizontal:16}}>
//           <View style={{alignItems: 'center', marginTop: 8, marginBottom: 18}}>
//               <View style={{width:35, height:2, backgroundColor:'#EAEAEA', marginVertical:2}}/>
//               <View style={{width:35, height:2, backgroundColor:'#EAEAEA', marginVertical:2}}/>
//           </View>
//           <View style={{flexDirection:'row', justifyContent:'space-between',alignItems:'center'}}>
//             <Text style={{fontSize:20, fontWeight:'bold', color:'black', marginLeft:10, color :'#191970'}}>Satya2GoQR</Text>
//             <Text style={{fontSize:14, fontWeight:'bold', color:'purple'}}>Details Item</Text>
//           </View>
//             {/* ScanResult */}
//           <View style={{flexDirection:'row', alignItems:'flex-start', marginTop:14, width:'100%'}}>
//             <View style={{alignItems:'center', width: 160, justifyContent:'center', paddingRight: 12}}>
//               <IconWithText 
//               img={require('../../../assets/icons/ScanQR1.png')}/>
//               <Text>Scan Lagi</Text>
//             </View>
//             <View style={{ width:1, height:110, backgroundColor:'#EAEAEA'}}/>
//             <View style={{flex:1, paddingLeft: 12}}>
//             <Text style={{fontSize:24, fontWeight:'bold', color:'black', textAlign:'center', color:'green'}}>
//             {` ${this.state.barcode}`}
//             </Text>
//             <Text style={{fontSize:24, fontWeight:'bold', color:'black', textAlign:'center', color:'green'}}>
//             {` ${this.state.dataSource}`}
//             </Text>
//             </View>
//           </View>
//         </View>          
//       </View>
//     )
//   }
// }


//export default ScanQode;