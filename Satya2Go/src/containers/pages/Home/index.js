import React, {Component} from 'react';
import {ScrollView, Text, View, StatusBar, Dimensions, StyleSheet, Image} from 'react-native';
import MainMenus from '../../organism/MainMenus';



export default class Home extends Component {
  render() {
    return (
      <View
      style={{
        flex: 1,
        justifyContent: 'flex-end',
      }}>
      <StatusBar
        barStyle='light-content'
        hidden={false}
        backgroundColor='#000'
      />
      {/* Background*/}
      <View style={{...StyleSheet.absoluteFill}}>
        <Image
          source={require('../../../assets/Image/bgSplash.png')}
          style={{flex: 1, height: null, width: null}}
          resizeMode='stretch'
        />
      </View>
      {/* Logo */}
      <View style={{flex:1, justifyContent:'flex-start',alignItems:'center', marginTop:25}}>
        <Image style={{width:110,height:110}}
        source={require('../../../assets/Image/Satya2Go.png')} />
      </View>
      <MainMenus />
      {/* Logo&Version info */}
      <View style={{flex:1, justifyContent:'flex-end',alignItems:'center',marginBottom:7}}>
        <Image 
        source={require('../../../assets/Image/SatyaSystem.png')} />
        <Text style={{fontSize:10, fontWeight:'bold', textAlign:'center', color:'#ffff'}}>Versi 0.1.0</Text>
      </View>
    </View>
  );
}
}

const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;

const styles = StyleSheet.create({
container: {
  flex: 1,
  alignItems: 'center',
  justifyContent: 'center',
},
button: {
  backgroundColor: 'white',
  height: 70,
  marginHorizontal: 20,
  borderRadius: 35,
  alignItems: 'center',
  justifyContent: 'center',
  marginVertical: 5,
},
});

