import React, { Component } from 'react';
import {View, StatusBar, Image , Text} from 'react-native'
import NavbarIcon from '../../organism/NavbarIcon/index';

class Inventory extends Component {
    render() {
        return(
        <View style={{flex:1, backgroundColor:'#FFF'}}>
        <StatusBar
        barStyle='dark-content'
        hidden={false}
        backgroundColor='#1E90FF'
      />
                <View style={{flex: 1, justifyContent:'center', alignItems:'center'}}>
                    <Image style={{justifyContent:'center', alignItems:'center', width:300, height: 300}} 
                    source={require('../../../assets/Image/banner.png')}/>
                    <Text style={{fontSize:20, fontWeight:'bold', textAlign:'center'}}>Selamat Datang</Text>
                </View>
                    <NavbarIcon />
            </View>
        )
    }

}

export default Inventory;


