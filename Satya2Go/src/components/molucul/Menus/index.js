import React from 'react';
import {View, Image,TouchableOpacity} from 'react-native';

const Menus = props => {
  return (
    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      <TouchableOpacity onPress={props.onPress}>
        <Image style={{width: 110, height: 110}} source={props.img} />
      </TouchableOpacity>
    </View>
  );
};
export default Menus;